<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserPostController extends Controller
{
    public function indexAction($page)
    {

        $r_Post = $this->getDoctrine()->getManager()->getRepository('AppBundle:OTab\Post');
        $posts  = $r_Post->findAll();

        return $this->render('AppBundle:UserPost:index.html.twig', array(
            'posts' => $posts
        ));
    }

    public function addAction()
    {
        return $this->render('AppBundle:UserPost:add.html.twig', array(
            // ...
        ));
    }

}
