<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$collection = new RouteCollection();

$collection->add('index', new Route('/', array(
    '_controller' => 'AppBundle:Default:index',
)));

$collection->add('index', new Route('/post/index/{page}/', array(
    '_controller' => 'AppBundle:UserPost:index',
)));

$collection->add('add', new Route('/post/add/', array(
    '_controller' => 'AppBundle:UserPost:add',
)));

return $collection;